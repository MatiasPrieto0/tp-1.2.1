using System;

namespace Ejercicio_2
{
    class Persona
    {
        private string SEXO_DEF = "H";
        private string nombre;
        private byte edad;
        private int DNI;
        public string sexo;
        private double peso;
        private double altura;
        
        public Persona()
        {
            this.nombre = "";
            this.edad = 0;
            this.DNI = generarDNI();
            this.sexo = SEXO_DEF;
            this.peso = 0;
            this.altura = 0;
        }
        public Persona(string nom, byte edad, string sex)
        {
            this.nombre = nom;
            this.edad = edad;
            this.DNI = generarDNI();
            this.sexo = sex;
            this.peso = 0;
            this.altura = 0;
            comprobarSexo();
        }
        public Persona(string nom, byte edad, string sex, double peso, double altura)
        {
            this.nombre = nom;
            this.edad = edad;
            this.DNI = generarDNI();
            this.sexo = sex;
            this.peso = peso;
            this.altura = altura;
            comprobarSexo();
        }
        public int calcularIMC()
        {
            int r = 0;
            double ideal = this.peso / ((this.altura/100) * (this.altura/100));
            if (ideal < 20)
            {
                r = -1;//bajo peso
            }
            if (ideal >= 20 && ideal <=25)
            {
                r =  0;//ideal
            }
            if (ideal > 25)
            {
                r = 1;//sobrepeso
            }
            return r;
        }
        public bool esMayorDeEdad()
        {
            if (this.edad < 18)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        private void comprobarSexo()
        {
            if(this.sexo != "H" && this.sexo != "M")
            {
                this.sexo = this.SEXO_DEF;
            }
        }
        private int generarDNI()
        {
            Random numRandom = new Random();
            int dni = numRandom.Next(10000000, 99999999);
            return dni;
        }
        public void setNombre(string nom)
        {
            this.nombre = nom;
        }
        public void setEdad(byte edad)
        {
            this.edad = edad;
        }
        public void setSexo(string sex)
        {
            this.sexo = sex;
        }
        public void setPeso(double peso)
        {
            this.peso = peso;
        }
        public void setAltura(double alt)
        {
            this.altura = alt;
        }
        public string toString()
        {
            string str = "Nombre: "+nombre+". Edad: "+edad+". DNI: "+DNI+". Sexo: "+sexo+". Peso: "+peso+"kg. Altura: "+altura+"cm.";
            return str;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("nombre:");
            string nombre = Console.ReadLine();
            Console.WriteLine("edad:");
            byte edad = byte.Parse(Console.ReadLine());
            Console.WriteLine("sexo(H/M):");
            string sexo = Console.ReadLine();
            Console.WriteLine("peso:");
            int peso = int.Parse(Console.ReadLine());
            Console.WriteLine("altura:");
            int altura = int.Parse(Console.ReadLine());
            Persona persona = new Persona(nombre, edad, sexo, peso, altura);
            Persona persona2 = new Persona(nombre, edad, sexo);
            Persona persona3 = new Persona();


            persona3.setNombre("juana");
            persona3.setEdad(35);
            persona3.setSexo("M");
            persona3.setPeso(60);
            persona3.setAltura(170);

            Console.WriteLine("");

            if (persona.calcularIMC() == -1)
            {
                Console.WriteLine("persona esta por debajo de su peso ideal");
            }else if(persona.calcularIMC() == 0)
            {
                Console.WriteLine("persona esta en su peso ideal");
            }else if (persona.calcularIMC() == 1)
            {
                Console.WriteLine("persona esta por encima de su peso ideal");
            }
            if (persona2.calcularIMC() == -1)
            {
                Console.WriteLine("persona2 esta por debajo de su peso ideal");
            }
            else if (persona2.calcularIMC() == 0)
            {
                Console.WriteLine("persona2 esta en su peso ideal");
            }
            else if (persona2.calcularIMC() == 1)
            {
                Console.WriteLine("persona2 esta por encima de su peso ideal");
            }
            if (persona3.calcularIMC() == -1)
            {
                Console.WriteLine("persona3 esta por debajo de su peso ideal");
            }
            else if (persona3.calcularIMC() == 0)
            {
                Console.WriteLine("persona3 esta en su peso ideal");
            }
            else if (persona3.calcularIMC() == 1)
            {
                Console.WriteLine("persona3 esta por encima de su peso ideal");
            }

            Console.WriteLine("");

            if (persona.esMayorDeEdad())
            {
                Console.WriteLine("persona es mayor de edad");
            }
            else
            {
                Console.WriteLine("persona es menor de edad");
            }
            if (persona2.esMayorDeEdad())
            {
                Console.WriteLine("persona2 es mayor de edad");
            }
            else
            {
                Console.WriteLine("persona2 es menor de edad");
            }
            if (persona3.esMayorDeEdad())
            {
                Console.WriteLine("persona3 es mayor de edad");
            }
            else
            {
                Console.WriteLine("persona3 es menor de edad");
            }

            Console.WriteLine("");

            Console.WriteLine("persona: " + persona.toString());
            Console.WriteLine("persona2: " + persona2.toString());
            Console.WriteLine("persona3: " + persona3.toString());
        }
    }
}

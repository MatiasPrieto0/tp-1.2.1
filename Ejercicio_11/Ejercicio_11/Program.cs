﻿using System;
using System.Collections.Generic;

namespace Ejercicio_11
{
    class Jugador
    {
        public int victorias = 0;
        public string nombre;
        public int dinero = 300;
        public Porra porra;
        public string prediccion = "";
        public int victorias_totales = 0;

        public Jugador(string nombre, Porra porra)
        {
            this.nombre = nombre;
            this.porra = porra;
        }

        public string generarPrediccion()
        {
            porra.depositar(10);
            dinero -= 10;
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            int local = rdn.Next(0, 4);
            int visitante = rdn.Next(0, 4);
            this.prediccion = String.Format("{0} - {1}", local, visitante);
            return this.prediccion;
        }
        
        
    }

    class Partido
    {
        public string resultado = "";
        private List<Jugador> apostadores = new List<Jugador>();

        public Partido(List<Jugador> a)
        {
            generarResultado();
            apostadores = a;
        }
        public void generarResultado()
        {
            Random rdn = new Random();
            int local = rdn.Next(0, 4);
            int visitante = rdn.Next(0, 4);
            this.resultado = String.Format("{0} - {1}", local, visitante);
        }
        public void esCorrecto()
        {
            int c = apostadores.Count;
            foreach(Jugador a in apostadores)
            {
                if (resultado == a.prediccion)
                {
                    a.victorias++;
                    a.victorias_totales++;
                }
                if (a.victorias > 1)
                {
                    a.dinero += a.porra.llevarse();
                    a.victorias = 0;
                }
            }
        }
        public void jugar()
        {
            generarResultado();
            foreach (Jugador a in apostadores)
            {
                if (a.dinero >= 10)
                {
                    a.generarPrediccion();
                    esCorrecto();
                    Console.WriteLine("{4}: prediccion: {0}, resultado: {1}, dinero restante: {2}, victorias para llevarse el pozo: {3} de 2, victorias totales: {5}", a.prediccion, this.resultado, a.dinero, a.victorias, a.nombre, a.victorias_totales);
                }
                else
                {

                }
            }
        }
    }

    class Porra
    {
        private int monto = 100;

        public Porra()
        {

        }

        public void depositar(int d)
        {
            this.monto += d;
        }
        public int llevarse()
        {
            int a = monto;
            monto = 0;
            return a;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Porra porra = new Porra();
            Jugador j = new Jugador("raul", porra);
            Jugador k = new Jugador("carlos", porra);
            Jugador h = new Jugador("juan", porra);
            Jugador g = new Jugador("pedro", porra);
            Jugador f = new Jugador("pablo", porra);
            List<Jugador> jugadores = new List<Jugador>();
            jugadores.Add(j);
            jugadores.Add(k);
            jugadores.Add(h);
            jugadores.Add(g);
            jugadores.Add(f);
            Partido partido = new Partido(jugadores);
            for(int i = 0; i < 20; i++)
            {
                Console.WriteLine("PARTIDO {0}", i);
                partido.jugar();
                Console.WriteLine(" ");
                Console.WriteLine(" ");
            }
            
            
            /*ConsoleKeyInfo tecla;
            for (; ; )
            {
                if (Console.KeyAvailable)
                {
                    tecla = Console.ReadKey();
                    if (tecla.Key == ConsoleKey.Enter)
                    {
                        System.Threading.Thread.Sleep(100);
                        partido.jugar();
                    }
                }
            }*/
        }
    }
}

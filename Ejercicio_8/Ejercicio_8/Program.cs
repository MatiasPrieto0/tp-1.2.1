﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;

namespace Ejercicio_8
{
    abstract class Persona
    {
        protected string nombre;
        protected char sexo;
        protected int edad;
        protected bool asistencia;

        public Persona(string nombre, char sexo, int edad)
        {
            this.nombre = nombre;
            this.sexo = sexo;
            this.edad = edad;
            this.disponibilidad();
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public char getSexo
        {
            get
            {
                return this.sexo;
            }
        }
        public int getEdad
        {
            get
            {
                return this.edad;
            }
        }
        public bool getAsistencia
        {
            get
            {
                return this.asistencia;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public char setSexo
        {
            set
            {
                this.sexo = value;
            }
        }
        public int setEdad
        {
            set
            {
                this.edad = value;
            }
        }
        public abstract void disponibilidad();
    }

    class Estudiante : Persona
    {
        private int nota;
        public Estudiante(string nombre, char sexo, int edad, int nota) : base(nombre, sexo, edad)
        {
            this.nota = nota;
        }
        public int getNota
        {
            get
            {
                return this.nota;
            }
        }
        public int setNota
        {
            set
            {
                this.nota = value;
            }
        }
        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 50) ? false : true;
        }
        public string toString()
        {
            return string.Format("Nombre: {0}, Sexo: {1}, Nota: {2}", this.nombre, this.sexo, this.nota);
        }
    }

    class Profesor : Persona
    {
        private string materia;
        public Profesor(string nombre, char sexo, int edad, string materia) : base(nombre, sexo, edad)
        {
            this.materia = materia;
        }
        public string getMateria
        {
            get
            {
                return this.materia;
            }
        }
        public string setMateria
        {
            set
            {
                this.materia = value;
            }
        }
        public override void disponibilidad()
        {
            var rnd = new Random(DateTime.UtcNow.Millisecond);
            float num = rnd.Next(0, 100);
            this.asistencia = (num < 20) ? false : true;
        }
    }
    class Aula
    {
        private int id;
        private Profesor profesor;
        private List<Estudiante> estudiantes = new List<Estudiante>();
        private string materia;

        public Aula(int id, List<string> nombres, List<char> sexo, List<int> edades, List<int> notas, Profesor profesor)
        {
            for (int i = 0; i < nombres.Count; i++)
            {
                this.estudiantes.Add(new Estudiante(nombres[i], sexo[i], edades[i], notas[i]));
            }
            this.id = id;
            this.profesor = profesor;
        }
        public bool asistenciaAlumnos()
        {
            int presentes = 0;
            foreach (Estudiante e in estudiantes)
            {
                e.disponibilidad();
                if (e.getAsistencia)
                {
                    presentes++;
                }
            }
            return (presentes >= Math.Round((decimal)estudiantes.Count / 2));
        }
        public bool darClase()
        {
            profesor.disponibilidad();
            if (!profesor.getAsistencia)
            {
                return false;
            }
            if (profesor.getMateria != this.materia)
            {
                Console.WriteLine("No se puede dar clase. El profesor es de otra materia.");
                return false;
            }
            if (!this.asistenciaAlumnos())
            {
                Console.WriteLine("No se puede dar clase. Asistencia insuficiente.");
                return false;
            }
            Console.WriteLine("Se puede dar clases.");
            return true;
        }
        public void notas()
        {
            int m = 0;
            int h = 0;
            foreach (Estudiante e in this.estudiantes)
            {
                if (e.getSexo == 'H')
                {
                    h++;
                }
                else if (e.getSexo == 'M')
                {
                    m++;
                }
                Console.WriteLine(e.toString());
            }
            Console.WriteLine("Hay {0} chicos y {1} chicas aprobados/as", h, m);
        }
    }
    class Ejecutable
    {
        static void Main(string[] args)
        {
            Profesor profe = new Profesor("Pablo", 'H', 40, "Computacion");
            Aula aula = new Aula(1, new List<string>() { "Juan", "Raul", "Pedro", "Maria" }, new List<char>() { 'H', 'H', 'H', 'M' }, new List<int>() { 16, 17, 16, 18 }, new List<int>() { 10, 5, 8, 6 }, profe);
            if (aula.darClase())
            {
                aula.notas();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejercicio_9
{
    class Pelicula
    {
        private string titulo;
        private int duracion;
        private int edad_minima;
        private string director;

        public Pelicula(string tit, int dur, int edad, string dir)
        {
            this.titulo = tit;
            this.duracion = dur;
            this.edad_minima = edad;
            this.director = dir;
        }
        public int getEdadMin()
        {
            return edad_minima;
        }
    }

    class Asiento
    {
        private char letra;
        private int fila;
        private Espectador espectador = null;

        public Asiento(char letra, int fila)
        {
            this.letra = letra;
            this.fila = fila;
        }
        public void setEspectador(Espectador e)
        {
            this.espectador = e;
        }
        public bool ocupado()
        {
            return (this.espectador != null);
        }
        public int getFila()
        {
            return this.fila;
        }
        public char getLetra()
        {
            return this.letra;
        }
    }

    class Espectador
    {
        private string nombre;
        private int edad;
        private int dinero;
        public Espectador(string nombre, int edad, int dinero)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.dinero = dinero;
        }
        public int getEdad()
        {
            return edad;
        }
        public int getDinero()
        {
            return dinero;
        }
    }

    class Cine
    {
        private Pelicula pelicula;
        private int precio;
        private List<List<Asiento>> asientos = new List<List<Asiento>>();

        public Cine(int filas, int columnas, int precio, Pelicula pelicula)
        {
            this.precio = precio;
            this.pelicula = pelicula;
            int columnas_t = (columnas > 26) ? 26 : columnas;
            for (int i = 0; i < filas; i++)
            {
                List<Asiento> fila_temp = new List<Asiento>();
                for (int z = 0; z < columnas_t; z++)
                {
                    fila_temp.Add(new Asiento((char)(z + 65), i + 1));
                }
                this.asientos.Add(fila_temp);
            }

        }
        public bool hayLugar()
        {
            foreach (List<Asiento> fila in asientos)
            {
                foreach (Asiento a in fila)
                {
                    if (a.ocupado() == false)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private int letra_col_to_index(char col)
        {
            int col_index = -1;
            if (((int)col >= 65 && (int)col <= 90) || (int)col >= 97 && (int)col <= 122)
            {
                if ((int)col <= 90)
                {
                    col_index = (int)col - 65;
                }
                else
                {
                    col_index = (int)col - 97;
                }
            }
            return col_index;
        }
        public bool sePuedeSentar(Espectador e)
        {
            return (e.getEdad()>pelicula.getEdadMin() && e.getDinero()>this.precio);
        }

        public List<Asiento> getButacasDisponibles()
        {
            List<Asiento> temp = new List<Asiento>();
            foreach (List<Asiento> fila in this.asientos)
            {
                foreach (Asiento a in fila)
                {
                    if (!a.ocupado())
                    {
                        temp.Add(a);
                    }
                }
            }
            return temp;
        }

        public void sentar(int fila, char col, Espectador e)
        {
            this.asientos[fila-1][letra_col_to_index(col)].setEspectador(e);
        }
        

    }
    class Program
    {
        static void Main(string[] args)
        {
            Pelicula pelicula = new Pelicula("Godzila vs Kong", 90, 16, "Adam Wingard");
            int filas = 8;
            int cols = 9;
            Console.WriteLine("Ingresar el precio de la entrada:");
            int precio = int.Parse(Console.ReadLine());
            Cine cine = new Cine(filas, cols, precio, pelicula);
            Console.WriteLine("Ingresar cantidad de espectadores:");
            int c_espectadores = int.Parse(Console.ReadLine());
            List<Espectador> espectadores = new List<Espectador>();
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            for (int i = 0; i < c_espectadores; i++)
            {
                espectadores.Add(new Espectador(i.ToString(), rdn.Next(5, 35), rdn.Next(0, 1000)));
                if (cine.hayLugar())
                {
                    if (cine.sePuedeSentar(espectadores.Last()))
                    {
                        List<Asiento> l = cine.getButacasDisponibles();
                        Asiento a = l[rdn.Next(0, l.Count)];
                        cine.sentar(a.getFila(), a.getLetra(), espectadores.Last());
                    }
                }
            }
        }
    }
}

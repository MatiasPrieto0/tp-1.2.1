﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ejercicio_16
{
    class Carta
    {
        private int numero;
        private string palo;
        private List<List<string>> palos = new List<List<string>> { new List<string> { "ORO", "COPA", "ESPADA", "BASTO" }, new List<string> { "DIAMANTE", "PICA", "CORAZON", "TREBOL" } };
        public Carta(int numero, string palo)
        {
            this.numero = numero;
            this.palo = palo;
        }
        public string toString()
        {
            int tipo = -1;
            for (int i = 0; i < 2; i++)
            {
                if (this.palos[i].IndexOf(this.palo) != -1)
                {
                    tipo = i;
                    break;
                }
            }
            switch (tipo)
            {
                case 0:
                    switch (this.numero)
                    {
                        case 1:
                            return String.Format("{0} de {1}", "As", this.palo);
                        case 10:
                            return String.Format("{0} de {1}", "Sota", this.palo);
                        case 12:
                            return String.Format("{0} de {1}", "Caballo", this.palo);
                        case 13:
                            return String.Format("{0} de {1}", "Rey", this.palo);
                        default:
                            return String.Format("{0} de {1}", this.numero, this.palo);
                    }
                case 1:
                    switch (this.numero)
                    {
                        case 1:
                            return String.Format("{0} de {1}", "As", this.palo);
                        case 11:
                            return String.Format("{0} de {1}", "Jota", this.palo);
                        case 12:
                            return String.Format("{0} de {1}", "Reina", this.palo);
                        case 13:
                            return String.Format("{0} de {1}", "Rey", this.palo);
                        default:
                            return String.Format("{0} de {1}", this.numero, this.palo);
                    }
                default:
                    return String.Format("{0} de {1}", this.numero, this.palo);
            }
        }
        public string getPalo
        {
            get
            {
                return this.palo;
            }
        }
    }
    abstract class Baraja
    {

        protected List<Carta> cartas = new List<Carta>();
        protected List<Carta> cartas_dadas = new List<Carta>();
        protected int cartas_total;
        protected int cartas_palo;
        protected string[] palos = new string[] { "ORO", "COPA", "ESPADA", "BASTO" };
        public Baraja() { }
        public abstract void crearBaraja();
        public void barajar()
        {
            Random rdn = new Random(DateTime.UtcNow.Millisecond);
            List<Carta> cartas_tmp = new List<Carta>();
            int c = this.cartas.Count;
            for (int i = 0; i < c; i++)
            {
                int pos = rdn.Next(this.cartas.Count);
                cartas_tmp.Add(this.cartas[pos]);
                this.cartas.RemoveAt(pos);
            }
            this.cartas = cartas_tmp;
        }
        public Carta siguienteCarta()
        {
            if (this.cartas.Count > 0)
            {
                this.cartas_dadas.Add(this.cartas[0]);
                this.cartas.RemoveAt(0);
                return this.cartas_dadas[this.cartas.Count - 1];
            }
            return null;
        }
        public Carta[] darCartas(int cant)
        {
            if (this.cartas.Count >= cant)
            {
                this.cartas_dadas.AddRange(cartas.Take(cant));
                this.cartas.RemoveRange(this.cartas.Count - 1 - cant, cant);
                return cartas_dadas.TakeLast(cant).ToArray();
            }
            return null;
        }
        public void cartasMonton()
        {
            if (this.cartas_dadas.Count == 0)
            {
                Console.WriteLine("No se dio ninguna carta.");
                return;
            }
            Console.WriteLine("Las cartas dadas son:");
            foreach (Carta carta in cartas_dadas)
            {
                Console.WriteLine(carta.toString());
            }
        }
        public void cartasBaraja()
        {
            if (this.cartas.Count == 0)
            {
                Console.WriteLine("No quedan mas cartas en la baraja.");
                return;
            }
            Console.WriteLine("Las cartas que quedan en la baraja son: ");
            foreach (Carta carta in cartas)
            {
                Console.WriteLine(carta.toString());
            }
        }
    }
    class BarajaEspañola : Baraja
    {
        bool ochosynueves;
        public BarajaEspañola(bool tieneochosynueves)
        {
            this.ochosynueves = tieneochosynueves;
            if (this.ochosynueves)
            {
                this.cartas_palo = 12;
            }
            else
            {
                this.cartas_palo = 10;
            }
            this.cartas_total = this.cartas_palo * this.palos.Length;
        }
        public override void crearBaraja()
        {
            foreach (string palo in this.palos)
            {
                if (this.ochosynueves)
                {
                    for (int i = 1; i < this.cartas_palo; i++)
                        this.cartas.Add(new Carta(i, palo));
                }
                else
                {
                    for (int i = 1; i < this.cartas_palo; i++)
                        this.cartas.Add(new Carta((i >= 8) ? i + 2 : i, palo));
                }
            }
        }


    }
    class BarajaFrancesa : Baraja
    {
        public BarajaFrancesa()
        {
            this.palos = new string[] { "DIAMANTE", "PICA", "CORAZON", "TREBOL" };
            this.cartas_palo = 52;
            this.cartas_total = this.cartas_palo * this.palos.Length;
        }
        public override void crearBaraja()
        {
            foreach (string palo in this.palos)
            {
                for (int i = 1; i < this.cartas_palo; i++)
                    this.cartas.Add(new Carta(i, palo));
            }
        }
        public bool cartaRoja(Carta c)
        {
            return (c.getPalo == palos[0] || c.getPalo == palos[2]);
        }
        public bool cartaNegra(Carta c)
        {
            return (c.getPalo == palos[1] || c.getPalo == palos[3]);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo tecla;
            BarajaEspañola b = new BarajaEspañola(true);
            b.crearBaraja();
            for (; ; )
            {
                if (Console.KeyAvailable)
                {
                    tecla = Console.ReadKey();
                    if (tecla.Key == ConsoleKey.Enter)
                    {
                        b.barajar();
                        System.Threading.Thread.Sleep(100);
                        Console.Clear();
                        b.darCartas(1);
                        b.cartasMonton();
                    }
                }
            }
        }
    }
}

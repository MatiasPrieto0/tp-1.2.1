﻿using System;

namespace Ejercicio_1
{
    public class Cuenta
    {
        private string titular;
        private double cantidad;

        public Cuenta(string titular)
        {
            this.titular = titular;
            this.cantidad = 0;
        }
        public Cuenta(string titular, double cantidad)
        {
            this.titular = titular;
            if (cantidad < 0)
            {
                this.cantidad = 0;
            }
            else
            {
                this.cantidad = cantidad;
            }
        }
        public string getTitular()
        {
            return titular;
        }
        public void setTitular(string titular)
        {
            this.titular = titular;
        }
        public double getCantidad()
        {
            return cantidad;
        }
        public void setCantidad(double cantidad)
        {
            this.cantidad = cantidad;
        }
        public string toString()
        {
            string text= "El titular " + titular + " tiene $" + cantidad + " en su cuenta";
            return text;
        }
        public void ingresar(double cantidad)
        {
            if (cantidad < 0)
            {
                this.cantidad += 0;
            }
            else
            {
                this.cantidad += cantidad;
            }
        }
        public void retirar(double cantidad)
        {
            if (this.cantidad - cantidad < 0)
            {
                this.cantidad = 0;
            }
            else
            {
                this.cantidad -= cantidad;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Cuenta acc;
            acc = new Cuenta("Raul", 500);
            acc.retirar(100);
            acc.ingresar(59600);
            Console.WriteLine(acc.toString());
            acc.setCantidad(777);
            Console.WriteLine(acc.toString());
            acc.setTitular("Samuel");
            Console.WriteLine(acc.toString());
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ejercicio_12_2_
{
    abstract class Empleado
    {
        protected string nombre;
        protected int edad;
        protected float salario;
        protected const int PLUS = 300;

        public Empleado(string nombre, int edad, float salario)
        {
            this.nombre = nombre;
            this.edad = edad;
            this.salario = salario;
        }
        public string getNombre
        {
            get
            {
                return this.nombre;
            }
        }
        public string setNombre
        {
            set
            {
                this.nombre = value;
            }
        }
        public int getEdad
        {
            get
            {
                return this.edad;
            }
        }
        public int setEdad
        {
            set
            {
                this.edad = value;
            }
        }
        public float getSalario
        {
            get
            {
                return this.salario;
            }
        }
        public float setSalario
        {
            set
            {
                this.salario = value;
            }
        }
        public string toString()
        {
            return String.Format("El empleado {0} tiene {1} años, su salario es {2}", this.nombre, this.edad, this.salario);
        }
        public abstract bool plus();
    }
    class Repartidor : Empleado
    {
        private string zona;
        public Repartidor(string nombre, int edad, float salario, string zona) : base(nombre, edad, salario)
        {
            this.zona = zona;
        }
        public override bool plus()
        {
            if (this.edad < 25 && this.zona == "zona 3")
            {
                this.salario += PLUS;
                return true;
            }
            return false;
        }
        public new string toString()
        {
            return String.Format("{0} y su zona es {1}", base.toString(), this.zona);
        }
    }
    class Comercial : Empleado
    {
        private double comision;
        public Comercial(string nombre, int edad, float salario, double comision) : base(nombre, edad, salario)
        {
            this.comision = comision;
        }
        public override bool plus()
        {
            if (this.edad > 30 && this.comision > 200)
            {
                this.salario += PLUS;
                return true;
            }
            return false;
        }
        public new string toString()
        {
            return String.Format("{0} y su comision es {1}", base.toString(), this.comision);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            List<Empleado> empleados = new List<Empleado>();
            empleados.Add(new Repartidor("Juan", 20, 250, "zona 1"));
            empleados.Add(new Comercial("Carlos", 40, 500, 250));
            empleados.Add(new Repartidor("Raul", 30, 300, "zona 2"));
            empleados.Add(new Comercial("Maria", 15, 350, 300));
            empleados.Add(new Repartidor("Sofia", 17, 400, "zona 3"));
            Console.WriteLine("Empleados antes del plus:");
            foreach (Empleado e in empleados)
            {
                Console.WriteLine(e.toString());
            }
            Console.WriteLine("Empleados despues del plus: ");
            foreach (Empleado e in empleados)
            {
                e.plus();
                Console.WriteLine(e.toString());
            }
        }
    }

}

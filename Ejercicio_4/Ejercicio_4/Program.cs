﻿using System;

namespace Ejercicio_4
{
    class Program
    {
        class Electrodomestico
        {
            private int precio_base = 100;
            private string color = "blanco";
            private char consumo = 'F';
            private int peso = 5;

            public Electrodomestico()
            {

            }
            public Electrodomestico(int precio, int peso)
            {
                this.precio_base = precio;
                this.peso = peso;
            }
            public Electrodomestico(int precio, string color, char consumo, int peso)
            {
                this.precio_base = precio;
                if(comprobarColor(color))
                { 
                    this.color = color;
                }
                if (comprobarConsumo(consumo))
                {
                    this.consumo = consumo;
                }
                this.peso = peso;
            }

            private bool comprobarColor(string color)
            {
                if (string.Equals(color, "blanco", StringComparison.OrdinalIgnoreCase) || string.Equals(color, "negro", StringComparison.OrdinalIgnoreCase) || string.Equals(color, "rojo", StringComparison.OrdinalIgnoreCase) || string.Equals(color, "azul", StringComparison.OrdinalIgnoreCase) || string.Equals(color, "gris", StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            private bool comprobarConsumo(char letra)
            {
                if (letra == 'A' || letra == 'B' || letra == 'C' || letra == 'D' || letra == 'E' || letra == 'F')
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public float precioFinal()
            {
                float precio = precio_base;
                switch (this.consumo)
                {
                    case 'A':
                        precio_base += 100;
                        break;
                    case 'B':
                        precio_base += 80;
                        break;
                    case 'C':
                        precio_base += 60;
                        break;
                    case 'D':
                        precio_base += 50;
                        break;
                    case 'E':
                        precio_base += 30;
                        break;
                    case 'F':
                        precio_base += 10;
                        break;
                }
                if (this.peso > 0 && this.peso < 19)
                {
                    this.peso += 10;
                }
                else if (this.peso > 20 && this.peso < 49)
                {
                    this.peso += 50;
                }
                else if (this.peso > 50 && this.peso < 79)
                {
                    this.peso += 80;
                }
                else if (this.peso > 80)
                {
                    this.peso += 100;
                }

                return precio;
            }
        }

        class Lavadora : Electrodomestico
        {
            private int carga = 5;
            public Lavadora() { }

            public Lavadora(int peso, int precio) : base(peso, precio) { }
            public Lavadora(int precio, string color, char consumo, int peso, int carga) : base(precio, color, consumo, peso)
            {
                this.carga = carga;
            }

            public int getCarga()
            {
                return this.carga;
            }
            public float precioFinal()
            {
                float precio = base.precioFinal();
                if (this.carga > 30)
                {
                    precio += 50;
                }
                return precio;
            }
        }

        class Television : Electrodomestico
        {
            private int resolucion = 20;
            private bool TDT = false;

            public Television() { }

            public Television(int peso, int precio) : base(peso, precio) { }
            public Television(int precio, string color, char consumo, int peso, int resolucion, bool tdt) : base(precio, color, consumo, peso)
            {
                this.resolucion = resolucion; 
                this.TDT = tdt;
            }
            public int getResolucion()
            {
                return this.resolucion;
            }
            public bool getTDT()
            {
                return this.TDT;
            }
            public float precioFinal()
            {
                float precio = base.precioFinal();
                if (this.TDT)
                {
                    precio += 50;
                }
                if (this.resolucion > 40)
                {
                    precio *= 1.30F;
                }
                return precio;
            }

        }

        static void Main(string[] args)
        {
            float precio = 0;
            float preciotv = 0;
            float preciolava = 0;
            Electrodomestico[] electrodomesticos = new Electrodomestico[10];
            electrodomesticos[0] = new Television(10, 15);
            electrodomesticos[1] = new Lavadora(15, 58);
            electrodomesticos[2] = new Television(10, "blanco", 'D', 45, 20, true);
            electrodomesticos[3] = new Lavadora(85, "gris", 'A', 32, 29);
            electrodomesticos[4] = new Lavadora();
            electrodomesticos[5] = new Television();
            electrodomesticos[6] = new Television(15, 65);
            electrodomesticos[7] = new Lavadora(34, 77);
            electrodomesticos[8] = new Lavadora(23, 32);
            electrodomesticos[9] = new Television(98, "rojo", 'B', 65, 47, false);
            int televisores = 0, lavarropas = 0;
            foreach (Electrodomestico e in electrodomesticos)
            {

                if (e is Television)
                {
                    preciotv += e.precioFinal();
                    televisores++;
                }
                if (e is Lavadora)
                {
                    preciolava += e.precioFinal();
                    lavarropas++;
                }
                precio += e.precioFinal();
            }
            Console.WriteLine("Televisores: {0} \nLavarropas: {1} \nElectrodomesticos: {2}", preciotv, preciolava, precio);
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Ejercicio_14
{
    class Bebida
    {
        protected int id;
        protected int litros;
        protected float precio;
        protected string marca;
        public Bebida(int id, int litros, float precio, string marca)
        {
            this.id = id;
            this.litros = litros;
            this.precio = precio;
            this.marca = marca;
        }
        public float getPrecio()
        {
            return this.precio;
        }
        public string getMarca()
        {
            return this.marca;
        }
        public int getId()
        {
            return this.id;
        }
        protected string toString()
        {
            return String.Format("La bebida con identificador {0}, de la marca {1}, tiene {2} litros, cuesta {3}", this.id, this.marca, this.litros, this.precio);
        }
    }
    class AguaMineral : Bebida
    {
        private string origen;
        public AguaMineral(int id, int litros, float precio, string marca, string origen) : base(id, litros, precio, marca)
        {
            this.origen = origen;
        }
        public new string toString()
        {
            return String.Format("{0} y su origen es {1}", base.toString(), this.origen);
        }
    }
    class BebidaAzucarada : Bebida
    {
        private float porcentaje_azucar;
        private bool promocion = false;
        private const float DESCUENTO = 0.9f;
        public BebidaAzucarada(int id, int litros, float precio, string marca, float porcentaje_azucar, bool promocion) : base(id, litros, precio, marca)
        {
            this.porcentaje_azucar = porcentaje_azucar;
            if (promocion)
            {
                this.promocion = true;
                this.precio *= DESCUENTO;
            }
        }
        public new string toString()
        {
            return String.Format("{0}, tiene un porcentaje de azucar de {1}% {2}", base.toString(), this.porcentaje_azucar, (this.promocion) ? "y se encuentra en promocion" : "");
        }
    }
    class Almacen
    {
        private List<List<Bebida>> estanteria = new List<List<Bebida>>();
        public Almacen(List<List<Bebida>> estanteria)
        {
            this.estanteria = estanteria;
        }
        public float precioDeTodasLasBebidas()
        {
            float total = 0;
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b != null)
                    {
                        total += b.getPrecio();
                    }
                }
            }
            return total;
        }
        public float precioPorMarca(string marca)
        {
            float total = 0;
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b != null)
                    {
                        if (b.getMarca() == marca)
                        {
                            total += b.getPrecio();
                        }
                    }
                }
            }
            return total;
        }
        public float calcularPrecioPorEstanteria(uint num_estanteria)
        {
            float total = 0;
            if (num_estanteria > this.estanteria.Count || num_estanteria == 0)
            {
                return 0;
            }
            foreach (List<Bebida> l in this.estanteria)
            {
                if (l[(int)num_estanteria - 1] != null)
                {
                    total += l[(int)num_estanteria - 1].getPrecio();
                }
            }
            return total;
        }
        public bool agregarProducto(Bebida z)
        {
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b.getId() == z.getId())
                    {
                        return false;
                    }
                }
            }
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b == null)
                    {
                        this.estanteria[this.estanteria.IndexOf(l)][l.IndexOf(b)] = z;
                        return true;
                    }
                }
            }
            return false;
        }
        public bool eliminarProductoById(int id)
        {
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b != null)
                    {
                        if (b.getId() == id)
                        {
                            this.estanteria[this.estanteria.IndexOf(l)][l.IndexOf(b)] = null;
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public void mostrarInformacion()
        {
            foreach (List<Bebida> l in this.estanteria)
            {
                foreach (Bebida b in l)
                {
                    if (b is AguaMineral)
                    {
                        AguaMineral a = (AguaMineral)b;
                        Console.WriteLine(a.toString());
                    }
                    else if (b is BebidaAzucarada)
                    {
                        BebidaAzucarada a = (BebidaAzucarada)b;
                        Console.WriteLine(a.toString());
                    }
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            
        }
    }
}

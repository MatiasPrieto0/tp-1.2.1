﻿using System;

namespace Ejercicio_5
{
    interface Entregable
    {
        void entregar();
        void devolver();
        bool isEntregado();
        Entregable compareTo(Entregable o);
    }


    class Serie : Entregable
    {
        private string titulo = "";
        private int num_temps = 3;
        private bool entregado = false;
        private string genero = "";
        private string creador = "";

        public Serie()
        {

        }
        public Serie(string tit, string creador)
        {
            this.titulo = tit;
            this.creador = creador;
        }
        public Serie(string tit, int temps, string genero, string creador)
        {
            this.titulo = tit;
            this.num_temps = temps;
            this.genero = genero;
            this.creador = creador;
        }
        public string getTitulo()
        {
            return this.titulo;
        }
        public int getTemps()
        {
            return this.num_temps;
        }
        public string getGenero()
        {
            return this.genero;
        }
        public string getCreador()
        {
            return this.creador;
        }

        public void setTitulo(string tit)
        {
            this.titulo = tit;
        }
        public void setTemps(int temps)
        {
            this.num_temps = temps;
        }
        public void setGenero(string genero)
        {
            this.genero = genero;
        }
        public void setCreador(string creador)
        {
            this.creador = creador;
        }


        public void devolver()
        {
            this.entregado = false;
        }
        public void entregar()
        {
            this.entregado = true;
        }
        public bool isEntregado()
        {
            return this.entregado;
        }

        public Entregable compareTo(Entregable o)
        {
            if (o is Serie)
            {
                Serie z = (Serie)o;
                if (z.getTemps() > this.num_temps)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if (o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if (z.getHoras() > this.num_temps)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }
    class Videojuego : Entregable
    {
        private string titulo = "";
        private int horas = 10;
        private bool entregado = false;
        private string genero = "";
        private string compañia = "";

        public Videojuego()
        {

        }
        public Videojuego(string tit, int horas)
        {
            this.titulo = tit;
            this.horas = horas;
        }
        public Videojuego(string tit, int horas, string genero, string compañia)
        {
            this.titulo = tit;
            this.horas = horas;
            this.genero = genero;
            this.compañia = compañia;
        }
        public string getTitulo()
        {
            return this.titulo;
        }
        public int getHoras()
        {
            return this.horas;
        }
        public string getGenero()
        {
            return this.genero;
        }
        public string getCompañia()
        {
            return this.compañia;
        }

        public void setTitulo(string tit)
        {
            this.titulo = tit;
        }
        public void setHoras(int horas)
        {
            this.horas = horas;
        }
        public void setGenero(string genero)
        {
            this.genero = genero;
        }
        public void setCompañia(string compañia)
        {
            this.compañia = compañia;
        }


        public void devolver()
        {
            this.entregado = false;
        }
        public void entregar()
        {
            this.entregado = true;
        }
        public bool isEntregado()
        {
            return this.entregado;
        }

        public Entregable compareTo(Entregable o)
        {
            if (o is Serie)
            {
                Serie z = (Serie)o;
                if (z.getTemps() > this.horas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            else if (o is Videojuego)
            {
                Videojuego z = (Videojuego)o;
                if (z.getHoras() > this.horas)
                {
                    return z;
                }
                else
                {
                    return this;
                }
            }
            return null;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Serie[] series = new Serie[5];
            Videojuego[] videojuegos = new Videojuego[5];
            series[0] = new Serie("La Serie", "Creador");
            series[1] = new Serie("Travelers", 3, "Ciencia ficcion", "Alguien");
            series[2] = new Serie("Charlotte", "Jun Maeda");
            series[3] = new Serie("Lost", 6, "Misterio", "J.J. Abrams");
            series[4] = new Serie("DRagon Ball", 5, "Anime", "Akira Toriyama");
            videojuegos[0] = new Videojuego("Minecraft", 20000);
            videojuegos[1] = new Videojuego("CSGO", 0);
            videojuegos[2] = new Videojuego("GTAV", 200);
            videojuegos[3] = new Videojuego("The Elder Scrolls V: Skyrim", 10000, "RPG", "Bethesda");
            videojuegos[4] = new Videojuego("Fallout", 50);
            series[4].entregar();
            videojuegos[2].entregar();
            videojuegos[1].entregar();
            series[3].entregar();
            series[0].entregar();
            int juegos_entregados = 0, series_entregadas = 0;
            Serie mas_larga = series[0];
            Videojuego mas_largo = videojuegos[0];
            for (int w = 0; w < 5; w++)
            {
                if (series[w].isEntregado())
                {
                    series_entregadas++;
                    series[w].devolver();
                }
                if (videojuegos[w].isEntregado())
                {
                    juegos_entregados++;
                    videojuegos[w].devolver();
                }
                if (series[w].getTemps() > mas_larga.getTemps())
                {
                    mas_larga = series[w];
                }
                if (videojuegos[w].getHoras() > mas_largo.getHoras())
                {
                    mas_largo = videojuegos[w];
                }
            }
            Console.WriteLine("{0} videojuegos fueron entregados y {1} series fueron entregadas.", juegos_entregados, series_entregadas);
            Console.WriteLine("La serie con mas temporadas es: {0}", mas_larga.getTitulo());
            Console.WriteLine("El juego con mas horas estimadas es: {0}", mas_largo.getTitulo());

        }
    }
}

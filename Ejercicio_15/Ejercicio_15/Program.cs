﻿using System;
using System.Collections.Generic;

namespace Ejercicio_15
{
    class Contacto
    {
        private string nombre;
        private int telefono;
        public Contacto(string nombre, int telefono)
        {
            this.nombre = nombre;
            this.telefono = telefono;
        }
        public string getNombre()
        {
            return this.nombre;
        }
        public int getTelefono()
        {
            return this.telefono;
        }
        public string toString()
        {
            return String.Format("Nombre: {0} Telefono: {1}", this.nombre, this.telefono);
        }
    }
    class Agenda
    {
        private List<Contacto> contactos = new List<Contacto>();
        private int tamaño;
        public Agenda(int tamaño)
        {
            this.tamaño = tamaño;
        }
        public bool añadirContacto(Contacto c)
        {
            if (this.contactos.Count >= tamaño)
            {
                Console.WriteLine("Agenda Llena. No se pueden agregar mas contacots.");
                return false;
            }
            foreach (Contacto x in this.contactos)
            {
                if (c.getNombre() == x.getNombre())
                {
                    Console.WriteLine("No se pueden ingresar contactos que ya existan.");
                    return false;
                }
            }
            this.contactos.Add(c);
            return true;
        }
        public bool existeContacto(Contacto c)
        {
            return (this.contactos.IndexOf(c) != -1);
        }
        public int buscarContacto(string nombre)
        {
            foreach (Contacto x in this.contactos)
            {
                if (x.getNombre() == nombre)
                {
                    return x.getTelefono();
                }
            }
            return -1;
        }
        public void listarContactos()
        {
            foreach (Contacto x in this.contactos)
            {
                Console.WriteLine(x.toString());
            }
        }
        public bool eliminarContacto(Contacto c)
        {
            if (this.contactos.Remove(c))
            {
                Console.WriteLine("Se ha eliminado el contacto: \n {0}", c.toString());
                return true;
            }
            Console.WriteLine("No se ha podido eliminar el contacto");
            return false;
        }
        public bool agendaLlena()
        {
            return (this.contactos.Count >= this.tamaño);
        }
        public int huecosLibres()
        {
            return (this.tamaño - this.contactos.Count);
        }
        public List<Contacto> getContactos()
        {
            return this.contactos;
        }
    }
    class Program
    {
        static bool salir = false;
        const byte tamMenu = 6;
        static Agenda agenda;
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese el tamaño para la agenda: ");
            int tamaño = int.Parse(Console.ReadLine());
            agenda = new Agenda(tamaño);
            while (!salir)
            {
                menu();
            }
        }
        static void menu()
        {
            Console.Clear();
            Console.WriteLine("1.Ingresar contacto \n" +
                              "2.Buscar contacto por nombre \n" +
                              "3.Mostrar contactos \n" +
                              "4.Eliminar contacto\n" +
                              "5.Espacios libres\n" +
                              "6.Salir");
            bool num = false;
            int seleccion = 0;
            while (!num)
            {
                string entrada = Console.ReadLine();
                if (int.TryParse(entrada, out seleccion))
                {
                    if (seleccion >= 1 && seleccion <= tamMenu)
                    {
                        num = true;
                    }
                    else
                    {
                        num_invalido();
                    }
                }
                else
                {
                    num_invalido();
                }
            }
            switch (seleccion)
            {
                case 1:
                    ingresar_contacto();
                    break;
                case 2:
                    buscar_contacto_por_nombre();
                    break;
                case 3:
                    mostrar_contactos();
                    break;
                case 4:
                    eliminar_contacto();
                    break;
                case 5:
                    espacios_libres();
                    break;
                case 8:
                    salir = true;
                    break;
            }
        }
        static void num_invalido()
        {
            Console.WriteLine("El numero ingresado no corresponde con ninguna de las opciones, por favor ingrese un numero valido");
            Console.ReadKey();
            menu();
        }
        static void ingresar_contacto()
        {
            Console.WriteLine("Ingrese el nombre para el nuevo contacto");
            string nom = Console.ReadLine();
            bool numero = false;
            int num = 0;
            while (!numero)
            {
                Console.WriteLine("Ingrese un numero para el contacto {0}", nom);
                numero = int.TryParse(Console.ReadLine(), out num);
            }
            agenda.añadirContacto(new Contacto(nom, num));
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadKey();
            menu();
        }
        static void buscar_contacto_por_nombre()
        {
            Console.WriteLine("Ingrese el nombre del contacto a buscar: ");
            string nombre = Console.ReadLine();
            int t = agenda.buscarContacto(nombre);
            if (t != -1)
            {
                Console.WriteLine("El numero del contacto {0} es {1}", nombre, t);
            }
            else
            {
                Console.WriteLine("No hay ningun contacto llamado {0}", nombre);
            }
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadKey();
            menu();
        }
        static void mostrar_contactos()
        {
            agenda.listarContactos();
            Console.WriteLine("Presione cualquier tecla para continuar");
            Console.ReadKey();
        }
        static void eliminar_contacto()
        {
            Console.WriteLine("Ingrese 0 para volver al menu principal.");
            for (int x = 0; x < agenda.getContactos().Count; x++)
            {
                Console.WriteLine("{0}. {1}", x + 1, agenda.getContactos()[x].toString());
            }
            bool num = false;
            int seleccion = 0;
            Console.WriteLine("Ingrese el numero del contacto que desea eliminar");
            while (!num)
            {
                string entrada = Console.ReadLine();
                if (int.TryParse(entrada, out seleccion))
                {
                    if (seleccion >= 0 && seleccion <= agenda.getContactos().Count)
                    {
                        num = true;
                    }
                    else
                    {
                        Console.WriteLine("El numero ingresado no corresponde con ninguna de las opciones, por favor ingrese un numero valido");
                        Console.ReadKey();
                        eliminar_contacto();
                        return;
                    }
                }
                else
                {
                    Console.WriteLine("El numero ingresado no corresponde con ninguna de las opciones, por favor ingrese un numero valido");
                    Console.ReadKey();
                    eliminar_contacto();
                    return;
                }
            }
            if (seleccion == 0)
            {
                return;
            }
            agenda.eliminarContacto(agenda.getContactos()[seleccion - 1]);
            menu();
        }
        static void espacios_libres()
        {
            Console.WriteLine("Quedan {0} lugares libres en la agenda.", agenda.huecosLibres());
            Console.ReadKey();
            menu();
        }
    }

}

﻿using System;
using System.Collections.Generic;

namespace Ejercicio_12
{
    class Revolver
    {
        private int pos_actual;
        private int pos_bala;

        public Revolver()
        {
            Random rdn = new Random();
            pos_actual = rdn.Next(0, 5);
            pos_bala = rdn.Next(0, 5);
        }
        public bool disparar()
        {
            if (this.pos_actual == this.pos_bala)
            {
                this.siguienteBala();
                return true;
            }
            this.siguienteBala();
            return false;
        }
        public void siguienteBala()
        {
            this.pos_actual = (pos_actual + 1) % 6;
        }
        public string toString()
        {
            return String.Format("La posicion del tambor es: {0} y la posicion de la bala es: {1}", this.pos_actual, this.pos_bala);
        }
    }

    class Jugador
    {
        private int id;
        public string nombre;
        private bool vivo = true;

        public Jugador(int id)
        {
            this.id = id;
            this.nombre = "Jugador " + id;
        }
        public bool getVivo()
        {
            return this.vivo;
        }
        public bool disparar(Revolver r)
        {
            this.vivo = !r.disparar();
            return !this.vivo;
        }
    }
    class Juego
    {
        List<Jugador> jugadores = new List<Jugador>();
        Revolver revolver = new Revolver();
        public Juego(int cantidad_jugadores)
        {
            if (cantidad_jugadores > 6)
            {
                cantidad_jugadores = 6;
            }
            int j = cantidad_jugadores;
            for (int z = 0; z < j; z++)
            {
                jugadores.Add(new Jugador(z + 1));
            }
        }
        public bool finJuego()
        {
            foreach (Jugador j in this.jugadores)
            {
                if (!j.getVivo())
                {
                    return true;
                }
            }
            return false;
        }

        public void ronda()
        {
            foreach (Jugador j in this.jugadores)
            {
                Console.Write("{0} se apunta y tira del gatillo. ", j.nombre);
                if (j.disparar(revolver))
                {
                    Console.WriteLine("La bale sale y lo mata");
                    Console.Write("{0} pierde. Fin del juego", j.nombre);
                    break;
                }
                Console.WriteLine("No sale bala");
            }
        }

    }



    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese la cantidad de jugadores (max 6)");
            int cant_j = int.Parse(Console.ReadLine());
            Juego juego = new Juego(cant_j);
            while (!juego.finJuego())
            {
                juego.ronda();
            }
        }
    }
}

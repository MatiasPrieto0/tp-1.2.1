﻿using System;

namespace Ejercicio_6
{
    class Libro
    {
        private int ISBN;
        private string titulo;
        private string autor;
        private int numero_paginas;

        public Libro(string autor, string titulo, int ISBN, int numero_paginas)
        {
            this.titulo = titulo;
            this.autor = autor;
            this.ISBN = ISBN;
            this.numero_paginas = numero_paginas;
        }
        public string getTitulo()
        {
            return this.titulo;
        }
        public int getNumeroPaginas()
        {
            return this.numero_paginas;
        }
        public int getISBN()
        {
            return this.ISBN;
        }
        public string toString()
        {
            string a = "El libro " + this.titulo + ", con ISBN " + this.ISBN + ", creado por " + this.autor + " tiene " + this.numero_paginas + " paginas.";
            return a;
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            Libro[] libros = new Libro[2];
            libros[0] = new Libro("Kaiu Shirai", "The Promised Neverland 1", 777777, 100);
            libros[1] = new Libro("Hirohiko Araki", "JoJo´s Bizzarre Adventure Part 1: Phantom Blood 1", 490386, 180);
            Libro mas_paginas = libros[0];
            for (int z = 0; z < libros.Length; z++)
            {
                Console.WriteLine(libros[z].toString());
                if (libros[z].getNumeroPaginas() > mas_paginas.getNumeroPaginas())
                {
                    mas_paginas = libros[z];
                }
            }
            Console.WriteLine("El libro con mas paginas es {0}", mas_paginas.getTitulo());

        }
    }
}
